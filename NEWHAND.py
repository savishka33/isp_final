from cProfile import label
import tkinter as tk 
from tkinter import *
from tkinter import ttk
from turtle import left, update 
from PIL import ImageTk, Image
import cv2
from matplotlib import image
import numpy as np
import mediapipe as mp
import time, datetime  #for delay functions
import microcontroller as mic
from ctypes.wintypes import RGB





window = Tk()
window.title("RoboCar control using Hand Gestures")
window.configure(bg = "black")
titleba = Label(window, text="Gestured control Robo Car", font=("Calibri",30), bg="black", 
            fg="white", relief=GROOVE).place(relx=0.5,rely=0.1, anchor ="center")



f1= LabelFrame(window, bg="red").place(relx=0.5,rely=0.5) #this is vid background
L1 = Label(f1, height = 500 , width = 660 ,bg="maroon")
L1.place(relx=0.5,rely=0.5, anchor = 'center') #ive placed it in center and u can change color too





v=StringVar()
str(v.get())


vidadd=StringVar() 
livevar=IntVar() #variable for live vid too, this depends on ur computer camera
livevar.set(0)


def video_click(address):
    global cap
    if address==0:
        cap = cv2.VideoCapture(0)
        vidadd.set(" ")


global direction
direction = 'Up'

Button(window,text="Start",bg="yellow",fg="black",command=lambda:video_click(livevar.get())).place(relx=0.05,rely=0.5)

def close():
    window.destroy()
#exit button 
Button(f1, text="Exit the Application", bg='#fffdd0', fg='black', font=("Calibri", 14, "bold"), command=close).place(relx=0.11, rely=0.8, anchor ="center")


mp_draw=mp.solutions.drawing_utils
mp_hand=mp.solutions.hands

tipIds=[4,8,12,16,20]



hands = mp_hand.Hands(min_detection_confidence=0.8, min_tracking_confidence=0.8)
cap = cv2.VideoCapture(0)


def Video():

        
        ret,img=cap.read()          # read the frame
        results=hands.process(img)   #make detections
        img.flags.writeable=True    #allows draw on the image
        img=cv2.cvtColor(img, cv2.COLOR_RGB2BGR)   #recoloring the frame




#rendering the results
        lmList=[]
        if results.multi_hand_landmarks:    #chechking whether if there's actually a result
            for hand_landmark in results.multi_hand_landmarks:    #looping through results of the multihand landmarks
                myHands=results.multi_hand_landmarks[0]
                for id, lm in enumerate(myHands.landmark):
                    h,w,c=img.shape       #adjust height, width and channel
                    cx,cy= int(lm.x*w), int(lm.y*h)
                    lmList.append([id,cx,cy])
                mp_draw.draw_landmarks(img, hand_landmark, mp_hand.HAND_CONNECTIONS)  #image, landmarks and coordinates

        fingers=[]   # to save finger count
        if len(lmList)!=0:
            if lmList[tipIds[0]][1] > lmList[tipIds[0]-1][1]:     #for thumb finger
                fingers.append(1)
            else:
                fingers.append(0)
            for id in range(1,5):
                if lmList[tipIds[id]][2] < lmList[tipIds[id]-2][2]:
                    fingers.append(1)   # finger is open
                else:
                    fingers.append(0)      #finger is closed
            total=fingers.count(1)     #count total fingers

            direction = mic.RCCAR(total)
         


            if total==1:
                                            #location, font, scale
                cv2.putText(img, "BRAKE", (45, 375), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), 5)
                print("brake")
            

            if total==5:

                cv2.putText(img, " FORWARD", (45, 375), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), 5)
                print("forward")

            if total==2:

                cv2.putText(img, " RIGHT", (45, 375), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), 5)
                print("right")


            if total==3:
                
                cv2.putText(img, " LEFT", (45, 375), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), 5)
                print("left")

            if total==4:
                
                cv2.putText(img, "REVERSE", (45, 375), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), 5)
                print("REVERSE")


         
            k=cv2.waitKey(1)
           
        return img
       

def select_img():
    image = Image.fromarray(Video())
    finalImage = ImageTk.PhotoImage(image)
    L1.configure(image=finalImage)
    L1.image = finalImage
    window.after(1, select_img)

select_img()

window.mainloop()